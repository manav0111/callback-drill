// Using callbacks and the fs module's asynchronous functions, do the following:
// 1. Create a directory of random JSON files
// 2. Delete those files simultaneously

const fs = require("fs");
const path=require("path");


const fsProblem1=(dirName,nooffiles)=>{
  fs.mkdir(dirName,(err)=>{
    if(err)
    {
      console.log(err);
      return;
    }
    else
    {
          // Generate random JSON files
          for (let i = 1; i <= nooffiles; i++) {
            const fileName = `file_${i}.json`;
            const filePath = path.join(dirName, fileName);
            const Data = { key: Math.random() };

            // Write random JSON data to files
            fs.writeFile(filePath, JSON.stringify(Data), (err) => {
              if (err) {
                console.log(err);
                return;
              }
              console.log(`${fileName} created`);
              fs.unlink(filePath,(err)=>{
                if(err)
                {
                  console.log(err);
                }
                else
                {
                  console.log(`${fileName} is Deleted`);

                }
               
              })


            });
          }
    }
  });
}

module.exports=fsProblem1;