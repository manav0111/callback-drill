/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/


const fs=require('fs');
const path = require('path');




// 1. Read the given file lipsum.txt

const ReadFile=()=>{
    const filepath=path.join(
        __dirname,
        "./lipsum.txt"
    )
    fs.readFile(filepath,'utf8',(data,err)=>{
        if(err)
        {
            console.log(err);
        }
        else
        {
            console.log(data);
    
        }
    })

}

// ReadFile();

// 2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
 

const uppercase=()=>{
    const filepath=path.join(
        __dirname,
        "./lipsum.txt"
    )
    fs.readFile(filepath,'utf8',(err,data)=>{
        
    if(err)
    {
        console.log(err);
        return;
    }

      console.log(data);
      let uppercaseData=data.toUpperCase();
      console.log(uppercaseData);

      fs.writeFile('newFile.txt',uppercaseData,(err)=>{
        if(err)
        {
            console.log(err)
            return;
        }
        console.log("New File Created and uppercase Data is inseted");


      });

      fs.writeFile('filesname','newFile.txt',(err)=>{
        console.log("FileName is appended");
      })
      

        
    });

}

// uppercase();

// Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt

const lowerCase=()=>{
    let filepath=path.join(
        __dirname,
        "test",
        "newFile.txt"
    )
    console.log(filepath);


    fs.readFile(filepath,"utf8",(err,data)=>{

        console.log(data);

        let lowerCaseText=data.toLowerCase();

        const sentences=lowerCaseText.split(' ');
        console.log(sentences);

        for(let index=0; index<sentences.length; index++)
        {
            filepath=path.join(
                __dirname,
                "test",
                "./newFile1.txt"
            )
            fs.appendFile(filepath,`${sentences[index]} ,`,(err)=>{
                console.log("Data inserted In newFile1.txt");
            })
        }

        filepath=path.join(
            __dirname,
            "test",
            "./filename"
        )
        fs.appendFile('filesname',',newFile1.txt',(err)=>{
            console.log("FileName is appended");
          })



    })
}

// lowerCase();

// 4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt

const SortData=()=>{
   let filepath=path.join(
        __dirname,
        "/test",
        "./newFile1.txt"
    )

    console.log(filepath);
    fs.readFile(filepath,'utf8',(err,data)=>{

        const sentences=data.split(' ');

        console.log(sentences);


        const sorteddata=sentences.sort((a, b) => a.toLowerCase() > b.toLowerCase() ? 1 : -1);
       
        console.log(sorteddata);

        for(let index=0; index<sorteddata.length; index++)
        {
          
            fs.appendFile('./newFile2.txt',`${sentences[index]},`,(err)=>{
                console.log("Data inserted In newFile2.txt");
            })
        }

      

        fs.appendFile('filesname',',newFile2.txt',(err)=>{
            console.log("FileName is appended");
          })



    })
}

// SortData();

const DeleteAll=()=>{
    let filepath=path.join(
        __dirname,
        "test",
        "/filesname"
       
    );

    console.log(filepath);
    fs.readFile(filepath,'utf8',(err,data)=>{

        console.log(data);

        const FilesNames=data.split(',');

        for(let index=0; index<FilesNames.length;index++)
        {
            fs.unlink(FilesNames[index],(err)=>{
                console.log(`File is Deleted ${FilesNames[index]}`);
            })
        }

    })  

}

DeleteAll();

const fsProblem2=()=>{
    ReadFile();
    uppercase();
    lowerCase();
    SortData();
    DeleteAll();
}


module.exports=fsProblem2;


